using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class ArraysExamples : MonoBehaviour
{
    public GameObject cubePrefab;

    public int rows;
    public int cols;

    public float speed = 3;

    GameObject[,] cubes;

    Vector3[,] headings;
    // Start is called before the first frame update
    void Start()
    {
        cubes = new GameObject[rows, cols];
        headings = new Vector3[rows, cols];

        for (int i = 0; i < rows; i++)
        {
            for (int j = 0; j < cols; j++)
            {
                cubes[i, j] = Instantiate(
                    cubePrefab,
                    new Vector3(i, 1, j),
                    Quaternion.identity
                );

                headings[i, j] = new Vector3(
                    Random.Range(-1f, 1f),
                    0,
                    Random.Range(-1f, 1f)
                );


            }
        }

    }

    // Update is called once per frame
    void Update()
    {
        for (int r = 0; r < rows; r++)
        {
            for (int c = 0; c < cols; c++)
            {
                cubes[r, c].transform.position += headings[r, c] * Time.deltaTime * speed;
            }
        }
    }
}
