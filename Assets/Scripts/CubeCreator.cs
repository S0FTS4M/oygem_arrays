using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class CubeCreator : MonoBehaviour
{
    public GameObject cubePrefab;

    public int rows=30;
    public int columns=30;
    public int depth=30;


    // Start is called before the first frame update
    void Start()
    {
        for (int r = 0; r < rows; r++)
        {
            for (int c = 0; c < columns; c++)
            {
                for (int d = 0; d < depth; d++)
                {
                    GameObject go = Instantiate(
                        cubePrefab,
                        new Vector3(r, d, c),
                        Quaternion.identity
                    );
                }
            }
        }
    }
}
