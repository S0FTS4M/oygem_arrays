using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class GroupMovementBehaviour : MonoBehaviour
{

    public GameObject capsulePrefab;

    public int objCount = 5;

    public float speed = 1;

    GameObject[] objects;
    Vector3[] headings;

    void Start()
    {
        objects = new GameObject[objCount];
        headings = new Vector3[objCount];

        headings[0] = Vector3.right;
        headings[1] = Vector3.left;
        headings[2] = Vector3.forward;
        headings[3] = Vector3.back;
        


        for (int i = 0; i < objects.Length; i++)
        {
            GameObject caps = Instantiate(
                capsulePrefab,
                Vector3.zero, //new Vector(0,0,1)
                Quaternion.identity
            );

            objects[i] = caps;
        }

    }

    void Update()
    {
        for (int i = 0; i < objects.Length; i++)
        {
            objects[i].transform.position += headings[i] * Time.deltaTime * speed;
        }
    }
}
