using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class SpawnerController : MonoBehaviour
{

    public Transform[] spawnPositions;

    public GameObject zombiePrefab;

    List<GameObject> zombies;

    List<float> speeds;

    float timer = 0;

    public float maxTime = 5;

    private void Start()
    {
        zombies = new List<GameObject>();
        speeds = new List<float>();
    }

    // Update is called once per frame
    void Update()
    {
        timer += Time.deltaTime;

        if (timer >= maxTime)
        {
            int index = Random.Range(0, spawnPositions.Length);
            Transform spawnPosTrans = spawnPositions[index];

            GameObject zombie = Instantiate(
                zombiePrefab,
                spawnPosTrans.position,
                Quaternion.identity
            );

            zombies.Add(zombie);

            speeds.Add(Random.Range(1f,5f));

            timer = 0;


        }

        for (int i = 0; i < zombies.Count; i++)
        {

            zombies[i].transform.position += Vector3.forward * Time.deltaTime * speeds[i];
        }

    }
}
